--- sales_revenue_by_category_qtr

CREATE OR REPLACE VIEW sales_revenue_by_category_qtr AS
SELECT 
    c.name AS category_name,
    SUM(p.amount) AS total_revenue
FROM 
    payment p
JOIN 
    rental r ON p.rental_id = r.rental_id
JOIN 
    inventory i ON r.inventory_id = i.inventory_id
JOIN 
    film f ON i.film_id = f.film_id
JOIN 
    film_category fc ON f.film_id = fc.film_id
JOIN 
    category c ON fc.category_id = c.category_id
WHERE 
    p.payment_date >= DATE_TRUNC('quarter', CURRENT_DATE)
    AND p.payment_date < DATE_TRUNC('quarter', CURRENT_DATE) + INTERVAL '3 months'
GROUP BY 
    c.name
HAVING 
    SUM(p.amount) > 0;
    
    
--- get_sales_revenue_by_category_qtr

CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(input_date DATE)
RETURNS TABLE (
    category_name TEXT,
    total_revenue NUMERIC
) AS $$
BEGIN
    RETURN QUERY
    SELECT 
        c.name AS category_name,
        SUM(p.amount) AS total_revenue
    FROM 
        payment p
    JOIN 
        rental r ON p.rental_id = r.rental_id
    JOIN 
        inventory i ON r.inventory_id = i.inventory_id
    JOIN 
        film f ON i.film_id = f.film_id
    JOIN 
        film_category fc ON f.film_id = fc.film_id
    JOIN 
        category c ON fc.category_id = c.category_id
    WHERE 
        p.payment_date >= DATE_TRUNC('quarter', input_date)
        AND p.payment_date < DATE_TRUNC('quarter', input_date) + INTERVAL '3 months'
    GROUP BY 
        c.name
    HAVING 
        SUM(p.amount) > 0;
END;
$$ LANGUAGE plpgsql;


--- new_movie

CREATE OR REPLACE FUNCTION new_movie(movie_title TEXT)
RETURNS VOID AS $$
DECLARE
    new_film_id INT;
    klingon_language_id INT;
BEGIN
    SELECT language_id INTO klingon_language_id
    FROM language
    WHERE name = 'Klingon';

    IF klingon_language_id IS NULL THEN
        RAISE EXCEPTION 'Language "Klingon" does not exist in the language table';
    END IF;

    SELECT COALESCE(MAX(film_id), 0) + 1 INTO new_film_id
    FROM film;

    INSERT INTO film (film_id, title, description, release_year, language_id, rental_duration, rental_rate, replacement_cost, last_update)
    VALUES (new_film_id, movie_title, '', EXTRACT(YEAR FROM CURRENT_DATE), klingon_language_id, 3, 4.99, 19.99, CURRENT_TIMESTAMP);

    RAISE NOTICE 'New movie % has been added with film_id %', movie_title, new_film_id;
END;
$$ LANGUAGE plpgsql;
